public class Main
{
    public static void main(String[] args)
    {
        Food[] allTheFood = new Food[5];
        allTheFood[0] = new Wings();
        allTheFood[0].setDescription("Wings");
        
        ///I can't call allTheFood[0].setRating(20) because allTheFood is a reference of type Food. 
        ///It can only access methods that are public in the class Food and public classes in
        ///inherits from Object
        ///In order to get this working, we need to cast allTheFood[0] to be of type Wings
        
        
        System.out.println(allTheFood[0].toString());
        
        System.out.println("Let's have a party. People better bring a lot of food.");
    }
}